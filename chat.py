from vertexai.preview.language_models import ChatModel, InputOutputTextPair
import streamlit as st


def science_tutoring(temperature: float = .2) -> None:

    chat_model = ChatModel.from_pretrained("chat-bison@001")

    # TODO developer - override these parameters as needed:
    parameters = {
        "temperature": temperature,  # Temperature controls the degree of randomness in token selection.
        "max_output_tokens": 256,    # Token limit determines the maximum amount of text output.
        "top_p": 0.95,               # Tokens are selected from most probable to least until the sum of their probabilities equals the top_p value.
        "top_k": 40,                 # A top_k of 1 means the selected token is the most probable among all tokens.
    }

    chat = chat_model.start_chat(
        context="My name is Tanuki. You are an astronomer, knowledgeable about the solar system.",
        examples=[
            InputOutputTextPair(
                input_text='How many moons does Mars have?',
                output_text='The planet Mars has two moons, Phobos and Deimos.',
            ),
        ]
    )

    prompt = st.text_input(label="", value="How many planets are there in the solar system?")
    response = chat.send_message(prompt, **parameters)
    print(f"Response from Model: {response.text}")
    st.write(response.text)
# [END aiplatform_sdk_chat]

    return response




if __name__ == "__main__":
    science_tutoring()